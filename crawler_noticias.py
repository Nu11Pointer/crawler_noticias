# exec(open("crawler_noticias.py").read())

import requests
from bs4 import BeautifulSoup

import json
import re
import os
import time
import hashlib
import sys
# from tqdm import tqdm

# Generación de URLs en base a una búsqueda de Google
busqueda = 'g20'
site = 'eleconomista.com.mx/'
num = '100' #maximo 100, no todos estan permitidos

save_path = busqueda + "_" + site.split(".")[0] + "/"

tiempo_sleep = 0 #Un numero muy bajo puede saturar el servidor

def filtrarLinks(soup):
	palabras_rechazadas = ["webcache","tags"]
	links = soup.findAll("a")
	linksNoticias = []
	for link in soup.find_all("a",href=re.compile("(?<=/url\?q=)(htt.*://.*)")):
		link = re.split(":(?=http)",link["href"].replace("/url?q=",""))
		if not any(palabra in link[0] for palabra in palabras_rechazadas):
		# if "webcache" not in link[0]: #elimina links feos
			linksNoticias.append(link[0])

	return linksNoticias;

'''
	Encuentra el elemento article que contiene la mayor cantidad de texto
'''
def encontrarArticle(soup):
	art_all = [tags.text for tags in soup.find_all("article")]
	return max(art_all)


def descargarNoticias(linksNoticias):
	print("Descargando "+str(len(linksNoticias))+" noticias de "+site)
	print('Se realizará una descarga pausada para no saturar el servidor.')
	
	if not os.path.exists(save_path):
	    os.makedirs(save_path)

	registros = []
	for url in linksNoticias: #tqdm(linksNoticias):

		page = requests.get(url)
		soup = BeautifulSoup(page.content, features="html5lib")

		#Comienza el procesamiento de la noticia, es un caso por cada noticia
		#Se deberá obtener titulo, cuerpo, identificador univoco y registro
		print("PROCESANDO URL: " + url)

		titulo = cuerpo = id_noticia = ""
		registro = {}

		if(site == 'ambito.com'):
			titulo, cuerpo, id_noticia, registro = noticiaAmbito(soup, url)
		elif(site == 'cronista.com'):
			titulo, cuerpo, id_noticia, registro = noticiaCronista(soup, url)
		elif("eleconomista" in site):
			titulo, cuerpo, id_noticia, registro = noticiaElEconomista(soup, url)
		
		if(titulo == None):
			print("OCURRIÓ UN ERROR CON LA NOTICIA ANTERIOR")
			continue

		print("TITULO: " + titulo)
		print("ID: " + id_noticia)
		print()
		#Fin del procesamiento de la noticia

		#Guardando archivos
		nombre_archivo = save_path + busqueda + "_" + registro["diario"] + "_" + id_noticia

		#Guardando HTML file
		guardarHTML(nombre_archivo, page)
		#Guardando txt file
		guardarTXT( nombre_archivo, titulo + "\n" + cuerpo)

		registros.append(registro)

		time.sleep(tiempo_sleep)

	#Grabar lista de registros
	jeyson = json.dumps(registros)
	f = open(save_path+"registro_noticias.json","w")
	f.write(jeyson)
	f.close()

"""
	Elimina un tag con una cierta clase
	En caso de no existir no lo elimina
	Funciona por referencia
"""
def eliminarClassTag(soup,tag):
	c = soup.find('div',{'class',tag}) #Elimina tags propios de la página
	if(c is not None):
		c.decompose()

def noticiaElEconomista(soup, url):
	titular = soup.find("div",{"class","entry-top galleryDesktop"})
	if(titular is None):
		return None, None, None, None
	titulo = titular.h1.text
	copete = titular.h3.text 

	cuerpo = soup.find("div", {"class": "entry-body"})

	#Eliminando impurezas
	eliminarTag(cuerpo, "script")
	eliminarClassTag(cuerpo, 'entry-tags') #Elimina tags propios de la página
	eliminarClassTag(cuerpo, 'nota-autor-opinion clearfix') #Editorial de un periodista guarda sus datos en pie de pagina
	eliminarClassTag(cuerpo, 'ck-twitter') #Tag twitter embebido
	cuerpo.noscript.decompose() #Pueden surgir errores

	cuerpo = eliminarCharIlegales(cuerpo.text)

	#Sumamos el copete al cuerpo
	cuerpo = copete + " " + cuerpo

	fecha = soup.find('span',{'class','article-date'})
	if(fecha is not None):
		fecha = eliminarCharIlegales(fecha.text)

	registro = {}
	registro['url'] = url
	registro['diario'] = 'eleconomista'
	registro['fecha'] = fecha
	registro['titulo'] = titulo
	registro['id'] = crearHash(url)

	return titulo, cuerpo, registro['id'], registro

def crearHash(s):
	return str(int(hashlib.sha1(s.encode('utf-8')).hexdigest(),16) % (10 ** 8))

def noticiaCronista(soup, url):
	#Accelerated Mobile Pages a comun
	if("/amp/" in url):
		url = url.replace("/amp/","/")

	page = requests.get(url)
	soup = BeautifulSoup(page.content, features="html5lib")

	titulo = soup.h1
	
	if(titulo is None):
		return None, None, None, None

	titulo = titulo.text
	titulo = eliminarCharIlegales(titulo)

	cuerpo = soup.find("div",{"class","content-txt"})
	eliminarTag(cuerpo,"script")
	cuerpo = eliminarCharIlegales(cuerpo.text)

	registro = {}
	registro['fecha'] = eliminarCharIlegales(soup.find("time",{"class","entry-date"}).text)
	registro['url'] = url
	registro['diario'] = 'cronista'
	registro['titulo'] = titulo
	registro['url_corta'] = url.split("&")[0]

	id_noticia = crearHash(registro['url_corta'])
	registro['id'] = id_noticia

	return titulo, cuerpo, id_noticia, registro

"""
	Se debe devolver titulo, cuerpo, id_noticia y registro
"""
def noticiaAmbito(soup, url):
	titulo = soup.find('h2').text
	id_noticia = str(obtenerIDURL(limpiarURL(url)))

	cuerpo = encontrarArticle(soup)

	if("setTimeout" in cuerpo):
		cuerpo = verificarCargaPeresoza(id_noticia)

	if(not cuerpo): #Vale tanto para None como para vacio
		return None, None, None, None
	cuerpo = eliminarCharIlegales(cuerpo)

	registro = {}
	registro['url'] = url
	registro['url_corta'] = limpiarURL(url)
	registro['diario'] = 'ambito'
	registro['id'] = id_noticia
	registro['fecha'] = eliminarCharIlegales(soup.find('span',{'class':'dia'}).text.replace('\t',''))
	registro['titulo'] = titulo

	return titulo, cuerpo, id_noticia, registro

def guardarTXT(txt_file_name, contenido):
	txt_file_name = txt_file_name + ".txt"
	txt_file = open(txt_file_name, "w", encoding='utf-8')
	txt_file.write(contenido)
	txt_file.close()

def guardarHTML(html_file_name, page):
	html_file_name = html_file_name + ".html"
	html_file = open(html_file_name, "w", encoding='utf-8')
	html_file.write(page.content.decode('utf-8'))
	html_file.close()


"""
	Algunas noticias cargan de forma perezosa en ambito financiero
	Permanecen en el dominio data.ambito.com
"""
def verificarCargaPeresoza(id_noticia):
	url = "http://data.ambito.com/diario/cuerpo_noticia.asp?id="+id_noticia
	page_perezosa = requests.get(url)
	soup = BeautifulSoup(page_perezosa.content, features="html5lib")
	s = soup.find('div',{'id':'contenido_data'})
	return s.text

"""
	Elimina un determinado tag
	Creo que funciona por referencia al soup
"""
def eliminarTag(soup, tag):
	[x.extract() for x in soup.findAll(tag)]

"""
	Elimina los \t y \n del string (y otros)
"""
def eliminarCharIlegales(s):
	chars_to_remove = ['\t', '\n', '\xa0']
	return re.sub("\\t|\\n","",s).replace("\xa0"," ")

"""
	Se elimina el texto innecesario que compone una URL de ambito financiero
	Se asumen entrada validad
"""
def limpiarURL(link):
	return link.split("-")[0] #se guarda solo con el numero de referencia de la noticia


"""
	Se obtiene el ID de una noticia de ambito financiero
	Se asumen entrada validada (URL limpia)
"""
def obtenerIDURL(link):
	L = link.split("/")
	return L[len(L)-1] #Retorna el ultimo elemento


def main():
	print('Realizando búsqueda en '+site)

	page = requests.get("https://www.google.com.ar/search?q="+busqueda+"&as_sitesearch="+site+"&num="+num)
	soup = BeautifulSoup(page.content, features="html5lib")

	linksNoticias = filtrarLinks(soup)
	for url in linksNoticias:
		print(url)

	descargarNoticias(linksNoticias)

	# sys.exit()


if __name__ == "__main__":
    main()